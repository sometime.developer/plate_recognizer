#!/bin/bash

NAME=stream
NET="--net=host"
MOUNT="--mount type=bind,source=$(pwd)/stream,target=/user-data"
#USER_ID="--user $(id -u):$(id -g)"
ENV="-e LICENSE_KEY=LxrQYdeSST -e TOKEN=e4e9054776a9e72d4b65d8ee40163a8c1f7d602c"

docker run -d --rm $NET $MOUNT --name $NAME $HOST $USER_ID $ENV platerecognizer/alpr-stream

webhookserver/server.py

docker kill $NAME
