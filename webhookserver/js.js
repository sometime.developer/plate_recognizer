function translateColor(en)
{
    var dict = {
        'WHITE' : 'БІЛИЙ',
        'BLACK' : 'ЧОРНИЙ',
        'RED' : 'ЧЕРВОНИЙ',
        'BLUE' : 'СИНІЙ',
        'GREEN' : 'ЗЕЛЕНИЙ',
        'SILVER' : 'СІРИЙ',
        'GREY' : 'СІРИЙ',
        'BROWN' : 'КОРИЧНЕВИЙ',
        'YELLOW' : 'ЖОВТИЙ'
    };

    return dict[en];
}

function getLog()
{
    // FIXME remake this using websockets
    theUrl = "/log";
    const XHR = new XMLHttpRequest();

    XHR.addEventListener( 'load', function(event) { console.log( 'Data sent and response loaded.' );
        var data = JSON.parse(event.target.responseText);
        console.log(data)
        var content = {}
        for (index = data.length - 1; index >= 0 ; index--)
        {
            var d = data[index];
            var source = "UI";

            if (d.source) {
                source = d.source;
            }

            if (!(source in content)) {
                content[source] = "";
            }

            content[source] += "<b>" + d.ts + "<b><br>";
            content[source] += "<label class='camname'>CAMERA:&emsp;" + source + "</label><br>"
            if (d.plate) {
                content[source] += "number:&emsp;" + d.plate + "<br>";
                content[source] += "<img alt='no photo' src='img/" + d.plate + ".jpg' /><br>";
            }

            if (d.result && d.opd.number) {
                var r = d.result;
                if (r.vehicle && r.model_make) {
                    var type = r.vehicle.type;
                    var make = r.model_make[0].make.toUpperCase();
                    var model = r.model_make[0].model;
                    var color = translateColor(r.color[0].color.toUpperCase());

                    var detected_car = type + " " + make + " " + model + " " + color;

                    var model_match = -1;
                    var color_match = -1;

                    var db_model = "unknown";
                    var db_color = "unknown";

                    if (d.opd.model) {
                        db_model = d.opd.model.toUpperCase();
                        model_match  = db_model.search(make);
                    }

                    if (d.opd.color) {
                        db_color = d.opd.color.toUpperCase();
                        color_match = db_color.search(color);
                    }

                    if (model_match >= 0) {
                        console.log("MODEL MATCH", db_model, detected_car);
                    } else {
                        console.log("MODEL MISMATCH", db_model, detected_car);
                        content[source] += "<label class='warning'>" + db_model + "</label><br>";
                    }

                    if (color_match >= 0) {
                        console.log("COLOR MATCH", db_color, color);
                    } else {
                        console.log("COLOR MISMATCH", db_color, color);
                        content[source] += "<label class='warning'>" + color + "/" + db_color + "</label><br>";
                    }

                    content[source] += "DETECTED:&emsp;" + detected_car + "<br>";
                }

                if (d.opd.body) {
                    content[source] += "DB:&emsp;" + d.opd.body + " ";
                }

                if (d.opd.kind) {
                    content[source] += d.opd.kind + " ";
                }

                if (d.opd.model) {
                    content[source] += d.opd.model + " ";
                }

                if (d.opd.color) {
                    content[source] += d.opd.color + " ";
                }

                if (d.opd.year) {
                    content[source] += d.opd.year + "<br>";
                }
            } else {
                content[source] += "NO DATA<br>";
            }


            if (d.local_file.plate) {
                content[source] += "<label class='warning'>!!!!!!!!" + d.local_file.status[0] + "!!!!!!!</label><br>";
            }

            content[source] += "<hr>";
        }

        for(var source in content) {
            log = document.getElementById(source + "-log");
            log.innerHTML = content[source];
        }
    } );

    // Define what happens in case of error
    XHR.addEventListener( 'error', function(event) { console.log( 'Oops! Something went wrong.' ); } );
    XHR.open( 'GET', '/log');
    XHR.send();
}

var input = document.getElementById("plate");

const btn = document.getElementById('send_button');

input.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        btn.click();
    }
});

function sendData() {
    data = "plate=" + document.getElementById("plate").value.toUpperCase()
    console.log( 'Sending data', data);

    const XHR = new XMLHttpRequest();

    XHR.open( 'POST', '');
    XHR.setRequestHeader( 'Content-Type', 'application/x-www-form-urlencoded' );
    XHR.send(data);
}

btn.addEventListener( 'click', sendData )
setInterval(getLog, 2000);
