#!/usr/bin/python3

from http.server import SimpleHTTPRequestHandler, HTTPServer
import json
import pandas as pd
import cgi
import requests
from json.decoder import JSONDecodeError
from urllib.parse import unquote_plus
from datetime import datetime
import base64
import queue

username="putin"
password="huilo"
folder="webhookserver"

_auth = base64.b64encode(f"{username}:{password}".encode()).decode()

class ServerHandler(SimpleHTTPRequestHandler):
    camlog = queue.Queue(20)

    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json;charset=UTF-8')
        self.end_headers()

    def log_request(self, code='-', size='-'):
        pass

    def log(self, data):
        if ServerHandler.camlog.full():
            ServerHandler.camlog.get()

        ServerHandler.camlog.put(data)
        print(str(data))

    def do_AUTHHEAD(self):
        self.send_response(401)
        self.send_header('WWW-Authenticate', 'Basic realm=\"Test\"')
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_POST(self):
        #print("POST request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
        self._set_response()

        ctype, pdict = cgi.parse_header(self.headers['Content-Type'])

        plates = []

        if ctype == 'multipart/form-data': # with image
            pdict['boundary'] = bytes(pdict['boundary'], 'utf-8')
            fields = cgi.parse_multipart(self.rfile, pdict)
            # Get webhook content
            json_data = json.loads(fields.get('json')[0])
            source = json_data['data']['camera_id']

            for r in json_data['data']['results']:
                del r['source_url']
                plate = (r['plate']).upper()
                plates.append((plate, r))

                buffer = fields.get('upload')[0]
                name = folder + "/img/" + plate + ".jpg"
                print('Saving image to %s' % name)
                with open(name, 'wb') as fp:
                    fp.write(buffer)
        else: # without image
            raw_data = self.rfile.read(int(self.headers['content-length'])).decode('utf-8')
            source = None

            if raw_data.startswith('json='):
                raw_data = unquote_plus(raw_data[5:])

                try:
                    json_data = json.loads(raw_data)
                    source = json_data['data']['camera_id']

                    for r in json_data['data']['results']:
                        del r['source_url']
                        plate = (r['plate']).upper()
                        plates.append((plate, r))
                except JSONDecodeError:
                    pass
            elif raw_data.startswith('plate='):
                source = "UI"
                raw_data = raw_data[6:]
                plates.append(raw_data)

        print(plates)
        self.check_plates(plates, source)

        self.wfile.write(b'OK')

    def check_plates(self, plates, source):
        for plate, result in plates:
            entry = {}

            entry["result"] = result
            entry["plate"] = plate
            entry["source"] = source
            entry["ts"] = datetime.now().strftime("%H:%M:%S (%d/%m/%Y)")
            entry["opd"] = self.check_opd(plate)
            entry["local_db"]  = self.check_local_db(plate)
            entry["local_file"] = self.check_local_file(plate)

            self.log(entry)

    def check_opd(self, plate):
        url = "https://opendatabot.com/api/v3/public/transport?number=" + plate
        r = requests.get(url = url)
        return r.json()

    def check_local_file(self, plate):
        data = pd.read_csv("local_db.csv", comment='#')
        res = data[data['plate'] == plate]

        rc = {}

        if len(res):
            rc = res.to_dict()

        return rc

    def check_local_db(self, plate):
        return {}

    def do_GET(self):
        #print("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
        global _auth

        if self.headers.get('Authorization') == None:
            self.do_AUTHHEAD()
            self.wfile.write(b"no auth header received")
        elif self.headers.get('Authorization') == 'Basic '+_auth:
            if self.path == "/log":
                self._set_response()
                lst = list(ServerHandler.camlog.queue)
                self.wfile.write((json.dumps(lst)).encode('utf-8'))
            else:
                self.directory = self.directory + "/" + folder #FIXME avoid this dirty hack
                return SimpleHTTPRequestHandler.do_GET(self)
        else:
            self.do_AUTHHEAD()
            self.wfile.write(b"not authenticated")

def run(server_class = HTTPServer, handler_class = ServerHandler, port = 8080):
    server_address = ("", port)
    httpd = server_class(server_address, handler_class)

    print("starting...")

    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass

    httpd.server_close()
    print("stopping...")

if __name__ == "__main__":
    from sys import argv

    if len(argv) == 2:
        run(port = int(argv[1]))
    else:
        run(port = 5555)
